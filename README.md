# WordDigger

Word game based on Paul Hudson’s word scramble. The goal is to quickly find words using the letters from a list of 8 letter words.