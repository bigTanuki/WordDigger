import SwiftUI

struct ContentView: View {
    
    @State private var usedWords = [String]()
    @State private var rootWord: String = ""
    @State private var newWord = ""
    @State private var highScore: Int = 0
    @State private var highScore2: Int = 0
    
    @State private var isPLayer1: Bool = true
    @State private var playerNames = ["Player 1", "Player 2"]
    @State private var playerName = "Player 1"
    @State private var currentPlayer = 0
    
    
    @State private var smallFont = true
    @State private var fontSize = 24.0
    @State private var scale = 1.0
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    
    var body: some View {
        NavigationStack {
            ZStack {
                Color(isPLayer1 ? .pink : .teal).ignoresSafeArea()
                List {
                    Section {
                        Text("Go \(playerName)!🧑🏽‍💻")
                            .frame(maxWidth: .infinity, alignment: .center)
                            .font(.custom("star jedi", size: smallFont ? 24 : 30))
                        //.font(smallFont ? .title : .largeTitle)
                            .onChange(of: playerName) {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    withAnimation(.linear(duration: 0.8).delay(0.2).repeatCount(5, autoreverses: true)) {
                                        smallFont.toggle()
                                    }
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
                                    withAnimation(.linear(duration: 0.8).delay(0.2)) {
                                        smallFont.toggle()
                                    }
                                    
                                }
                            }
                        
                            .padding(.vertical)
                        HStack {
                            Text("High Score: \(highScore) 🏆")
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .foregroundStyle(.brown)
                            Text("High Score: \(highScore2) 🏆")
                                .frame(maxWidth: .infinity, alignment: .trailing)
                                .foregroundStyle(.brown)
                        }
                        
                    }
                    
                    
                    Section ("The root word is ...") {
                        Text(rootWord)
                            .frame(maxWidth: .infinity, alignment: .center)
                            .font(.largeTitle.bold())
                            .padding()
                        Button("♨️ Press to Restart") {
                            startGame()
                        }
                        .frame(maxWidth: .infinity, alignment: .trailing)
                        .foregroundStyle(.red)
                    }
                    
                    
                    Section {
                        TextField("Enter your word", text: $newWord)
                            .textInputAutocapitalization(.never)
                        //.foregroundStyle(isPLayer1 ? .pink : .teal)
                        
                    }
                    
                    Section {
                        ForEach(usedWords, id: \.self) { word in
                            HStack {
                                Image(systemName: "\(word.count).circle")
                                Text(word)
                            }
                        }
                    }
                }
            }
            
            .onSubmit() {
                addNewWord()
            }
            .onAppear(perform: {
                startGame()
            })
            .alert(errorTitle, isPresented: $showingError) {
                Button("OK") {}
            } message: {
                Text(errorMessage)
            }
        }.scrollContentBackground(.hidden)
        
    } // bodyEnd
    
    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
        
        guard answer.count > 0 else {return}
        
        guard isOriginal(word: answer) else {
            wordConflict(title: "Word used already", message: "Be more original")
            return
        }
        guard isPossible(word: answer) else {
            wordConflict(title: "Word not possible", message: "You cannot spell that word from '\(rootWord)'!")
            return
        }
        guard isReal(word: answer) else {
            wordConflict(title: "Word not recognized", message: "You cannot just make up words you know!")
            return
        }
        
        guard isRoot(word: answer) else {
            wordConflict(title: "Rootword used", message: "You cannot use the  original word")
            return
        }
        
        guard isTooSmall(word: answer) else {
            wordConflict(title: "Too small", message: "You need to use a word with at least 3 letters")
            return
        }
        
        if answer.count > highScore {
            if currentPlayer == 0 {
                highScore = answer.count
            } else {
                highScore2 = answer.count
            }
            
        }
        
        withAnimation {
            usedWords.insert(answer, at: 0)
        }
        newWord = ""
        isPLayer1.toggle()
        
        
        if currentPlayer < playerNames.count-1 {
            currentPlayer += 1
        } else {
            currentPlayer = 0
        }
        playerName = playerNames[currentPlayer]
    }
    
    func startGame() {
        if let startWordsURL = Bundle.main.url(forResource: "words", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                let allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ?? "silkworm"
                usedWords = []
                return
            }
        }
        
        
        fatalError("Could not load start.txt from the bundle.")
        
    }
    
    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        
        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }
        
        return true
    }
    
    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        
        return misspelledRange.location == NSNotFound
    }
    
    func isRoot(word: String) -> Bool {
        word != rootWord
    }
    
    func isTooSmall(word: String) -> Bool {
        word.count >= 3
    }
    
    
    func wordConflict(title: String, message: String) {
        errorTitle = title
        errorMessage = message
        showingError = true
    }
    
}// ContentViewEnd

